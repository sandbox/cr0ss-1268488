<?php

/**
 * Implements hook_tokens().
 */
function token_termspath_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  
  $key = array_shift(array_keys($tokens));
  if (strstr($key, 'termspath:') !== FALSE && !empty($data['node'])) {
    if(!empty($options['sanitize'])) {
       $sanitize = $options['sanitize'];
    } else {
      $sanitize = FALSE;
    } 

    $node = $data['node'];
    $field = str_replace('termspath:', '', $key);
    $replacements[$tokens[$key]] = $sanitize ? check_plain(token_termspath_get_termspath($node, $field)) : token_termspath_get_termspath($node, $field);
  }

  return $replacements;
}

/**
 * Implements hook_token_info().
 */
function token_termspath_token_info() {
  $info = array();

  $info['tokens']['node']['termspath'] = array(
    'name' => t('Termspath'),
    'description' => t('Compiles the terms hierarchy token for a given node depending on vocabulary.'),
    'type' => 'termspath',
  );

  $info['types']['termspath'] = array(
    'name' => t('Termspath token'),
    'description' => t('Tokens related to termspath.'),
    'needs-data' => 'termspath',
  );

  $options = array('type' => 'taxonomy_term_reference');
  $term_refs_fields = field_read_fields($options);
  foreach($term_refs_fields as $field_name => $field) {
    $voc = taxonomy_vocabulary_machine_name_load($field['settings']['allowed_values'][0]['vocabulary']);
    $info['tokens']['termspath'][$field_name] = array(
      'name' => $voc->name,
      'description' => t('The hierarchy path for %name terms reference.', array('%name' => $voc->name)),
    );
  }

  return $info;
}

function token_termspath_get_termspath($node, $field) {
  $pathauto = '';  

  if(isset($node->{$field}) && !empty($node->{$field}['und'])) {
    $tid = $node->{$field}['und'][0]['tid'];
    $parents = array_reverse(taxonomy_get_parents_all($tid));
    $parents_names = array();
    foreach($parents as $term) {
      $parents_names[] = strtolower(str_replace(" ", "-", $term->name));
    }
   $pathauto = join("/", $parents_names);
  }

  return $pathauto;
}